﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFCore2.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "cliente",
                columns: table => new
                {
                    cliente_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    first_name = table.Column<string>(type: "varchar(45)", unicode: false, maxLength: 45, nullable: false),
                    last_name = table.Column<string>(type: "varchar(45)", unicode: false, maxLength: 45, nullable: false),
                    email = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__cliente__47E34D64D831072B", x => x.cliente_id);
                });

            migrationBuilder.CreateTable(
                name: "empleado",
                columns: table => new
                {
                    empleado_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    first_name = table.Column<string>(type: "varchar(45)", unicode: false, maxLength: 45, nullable: false),
                    last_name = table.Column<string>(type: "varchar(45)", unicode: false, maxLength: 45, nullable: false),
                    email = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    username = table.Column<string>(type: "varchar(16)", unicode: false, maxLength: 16, nullable: false),
                    password = table.Column<string>(type: "varchar(40)", unicode: false, maxLength: 40, nullable: true),
                    last_update = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__empleado__6FBB65FD02B31D5F", x => x.empleado_id);
                });

            migrationBuilder.CreateTable(
                name: "linea_producto",
                columns: table => new
                {
                    lineaproducto_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(type: "text", nullable: true),
                    last_update = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__linea_pr__10B40BAB0D7205C4", x => x.lineaproducto_id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedTimestamp = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "('0001-01-01T00:00:00.0000000')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "producto",
                columns: table => new
                {
                    producto_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    lineaproducto_id = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "varchar(45)", unicode: false, maxLength: 45, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__producto__FB5CEEEC5AD81BF5", x => x.producto_id);
                    table.ForeignKey(
                        name: "fk_inventory_film",
                        column: x => x.lineaproducto_id,
                        principalTable: "linea_producto",
                        principalColumn: "lineaproducto_id");
                });

            migrationBuilder.CreateTable(
                name: "orden_venta",
                columns: table => new
                {
                    ordenventa_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    producto_id = table.Column<int>(type: "int", nullable: false),
                    cliente_id = table.Column<int>(type: "int", nullable: false),
                    return_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    empleado_id = table.Column<int>(type: "int", nullable: false),
                    last_update = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__orden_ve__C91F3F47EC6A571B", x => x.ordenventa_id);
                    table.ForeignKey(
                        name: "fk_rental_customer",
                        column: x => x.cliente_id,
                        principalTable: "cliente",
                        principalColumn: "cliente_id");
                    table.ForeignKey(
                        name: "fk_rental_inventory",
                        column: x => x.producto_id,
                        principalTable: "producto",
                        principalColumn: "producto_id");
                    table.ForeignKey(
                        name: "fk_rental_staff",
                        column: x => x.empleado_id,
                        principalTable: "empleado",
                        principalColumn: "empleado_id");
                });

            migrationBuilder.CreateIndex(
                name: "idx_last_name",
                table: "cliente",
                column: "last_name");

            migrationBuilder.CreateIndex(
                name: "idx_title",
                table: "linea_producto",
                column: "title");

            migrationBuilder.CreateIndex(
                name: "idx_fk_customer_id",
                table: "orden_venta",
                column: "cliente_id");

            migrationBuilder.CreateIndex(
                name: "idx_fk_inventory_id",
                table: "orden_venta",
                column: "producto_id");

            migrationBuilder.CreateIndex(
                name: "idx_fk_staff_id",
                table: "orden_venta",
                column: "empleado_id");

            migrationBuilder.CreateIndex(
                name: "idx_fk_film_id",
                table: "producto",
                column: "lineaproducto_id");

            migrationBuilder.CreateIndex(
                name: "idx_store_id_film_id",
                table: "producto",
                column: "lineaproducto_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "orden_venta");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "cliente");

            migrationBuilder.DropTable(
                name: "producto");

            migrationBuilder.DropTable(
                name: "empleado");

            migrationBuilder.DropTable(
                name: "linea_producto");
        }
    }
}
