using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFCore2.Entities;

namespace EFCore2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LineaProductosController : ControllerBase
    {
        private readonly DecoWraps1Context _context;

        public LineaProductosController(DecoWraps1Context context)
        {
            _context = context;
        }

        // GET: api/LineaProductos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LineaProducto>>> GetLineaProductos()
        {
          if (_context.LineaProductos == null)
          {
              return NotFound();
          }
            return await _context.LineaProductos.ToListAsync();
        }

        // GET: api/LineaProductos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LineaProducto>> GetLineaProducto(int id)
        {
          if (_context.LineaProductos == null)
          {
              return NotFound();
          }
            var lineaProducto = await _context.LineaProductos.FindAsync(id);

            if (lineaProducto == null)
            {
                return NotFound();
            }

            return lineaProducto;
        }

        // PUT: api/LineaProductos/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLineaProducto(int id, LineaProducto lineaProducto)
        {
            if (id != lineaProducto.LineaproductoId)
            {
                return BadRequest();
            }

            _context.Entry(lineaProducto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LineaProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LineaProductos
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LineaProducto>> PostLineaProducto(LineaProducto lineaProducto)
        {
          if (_context.LineaProductos == null)
          {
              return Problem("Entity set 'DecoWraps1Context.LineaProductos'  is null.");
          }
            _context.LineaProductos.Add(lineaProducto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLineaProducto", new { id = lineaProducto.LineaproductoId }, lineaProducto);
        }

        // DELETE: api/LineaProductos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLineaProducto(int id)
        {
            if (_context.LineaProductos == null)
            {
                return NotFound();
            }
            var lineaProducto = await _context.LineaProductos.FindAsync(id);
            if (lineaProducto == null)
            {
                return NotFound();
            }

            _context.LineaProductos.Remove(lineaProducto);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LineaProductoExists(int id)
        {
            return (_context.LineaProductos?.Any(e => e.LineaproductoId == id)).GetValueOrDefault();
        }
    }
}
