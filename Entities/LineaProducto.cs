﻿using System;
using System.Collections.Generic;

namespace EFCore2.Entities;

public partial class LineaProducto
{
    public int LineaproductoId { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public byte[]? LastUpdate { get; set; }

    public virtual ICollection<Producto> Productos { get; set; } = new List<Producto>();
}
