﻿using System;
using System.Collections.Generic;

namespace EFCore2.Entities;

public partial class Post
{
    public int Id { get; set; }

    public string Content { get; set; } = null!;

    public DateTime CreatedTimestamp { get; set; }
}
