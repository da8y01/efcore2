﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFCore2.Entities;

public partial class DecoWraps1Context : DbContext
{
    public DecoWraps1Context()
    {
    }

    public DecoWraps1Context(DbContextOptions<DecoWraps1Context> options)
        : base(options)
    {
    }

    public virtual DbSet<Cliente> Clientes { get; set; }

    public virtual DbSet<Empleado> Empleados { get; set; }

    public virtual DbSet<LineaProducto> LineaProductos { get; set; }

    public virtual DbSet<OrdenVentum> OrdenVenta { get; set; }

    public virtual DbSet<Post> Posts { get; set; }

    public virtual DbSet<Producto> Productos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=DESKTOP-2AS75HO; Initial Catalog=DecoWraps1; Persist Security Info=True; MultipleActiveResultSets=True; User ID=sa; Password=SQLServer2022.; encrypt=false");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Cliente>(entity =>
        {
            entity.HasKey(e => e.ClienteId).HasName("PK__cliente__47E34D64D831072B");

            entity.ToTable("cliente");

            entity.HasIndex(e => e.LastName, "idx_last_name");

            entity.Property(e => e.ClienteId).HasColumnName("cliente_id");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("email");
            entity.Property(e => e.FirstName)
                .HasMaxLength(45)
                .IsUnicode(false)
                .HasColumnName("first_name");
            entity.Property(e => e.LastName)
                .HasMaxLength(45)
                .IsUnicode(false)
                .HasColumnName("last_name");
        });

        modelBuilder.Entity<Empleado>(entity =>
        {
            entity.HasKey(e => e.EmpleadoId).HasName("PK__empleado__6FBB65FD02B31D5F");

            entity.ToTable("empleado");

            entity.Property(e => e.EmpleadoId).HasColumnName("empleado_id");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("email");
            entity.Property(e => e.FirstName)
                .HasMaxLength(45)
                .IsUnicode(false)
                .HasColumnName("first_name");
            entity.Property(e => e.LastName)
                .HasMaxLength(45)
                .IsUnicode(false)
                .HasColumnName("last_name");
            entity.Property(e => e.LastUpdate)
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("last_update");
            entity.Property(e => e.Password)
                .HasMaxLength(40)
                .IsUnicode(false)
                .HasColumnName("password");
            entity.Property(e => e.Username)
                .HasMaxLength(16)
                .IsUnicode(false)
                .HasColumnName("username");
        });

        modelBuilder.Entity<LineaProducto>(entity =>
        {
            entity.HasKey(e => e.LineaproductoId).HasName("PK__linea_pr__10B40BAB0D7205C4");

            entity.ToTable("linea_producto");

            entity.HasIndex(e => e.Title, "idx_title");

            entity.Property(e => e.LineaproductoId).HasColumnName("lineaproducto_id");
            entity.Property(e => e.Description)
                .HasColumnType("text")
                .HasColumnName("description");
            entity.Property(e => e.LastUpdate)
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("last_update");
            entity.Property(e => e.Title)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("title");
        });

        modelBuilder.Entity<OrdenVentum>(entity =>
        {
            entity.HasKey(e => e.OrdenventaId).HasName("PK__orden_ve__C91F3F47EC6A571B");

            entity.ToTable("orden_venta");

            entity.HasIndex(e => e.ClienteId, "idx_fk_customer_id");

            entity.HasIndex(e => e.ProductoId, "idx_fk_inventory_id");

            entity.HasIndex(e => e.EmpleadoId, "idx_fk_staff_id");

            entity.Property(e => e.OrdenventaId).HasColumnName("ordenventa_id");
            entity.Property(e => e.ClienteId).HasColumnName("cliente_id");
            entity.Property(e => e.EmpleadoId).HasColumnName("empleado_id");
            entity.Property(e => e.LastUpdate)
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnName("last_update");
            entity.Property(e => e.ProductoId).HasColumnName("producto_id");
            entity.Property(e => e.ReturnDate)
                .HasColumnType("datetime")
                .HasColumnName("return_date");

            entity.HasOne(d => d.Cliente).WithMany(p => p.OrdenVenta)
                .HasForeignKey(d => d.ClienteId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_rental_customer");

            entity.HasOne(d => d.Empleado).WithMany(p => p.OrdenVenta)
                .HasForeignKey(d => d.EmpleadoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_rental_staff");

            entity.HasOne(d => d.Producto).WithMany(p => p.OrdenVenta)
                .HasForeignKey(d => d.ProductoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_rental_inventory");
        });

        modelBuilder.Entity<Post>(entity =>
        {
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Content).HasColumnName("content");
            entity.Property(e => e.CreatedTimestamp).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");
        });

        modelBuilder.Entity<Producto>(entity =>
        {
            entity.HasKey(e => e.ProductoId).HasName("PK__producto__FB5CEEEC5AD81BF5");

            entity.ToTable("producto");

            entity.HasIndex(e => e.LineaproductoId, "idx_fk_film_id");

            entity.HasIndex(e => e.LineaproductoId, "idx_store_id_film_id");

            entity.Property(e => e.ProductoId).HasColumnName("producto_id");
            entity.Property(e => e.LineaproductoId).HasColumnName("lineaproducto_id");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .IsUnicode(false)
                .HasColumnName("name");

            entity.HasOne(d => d.Lineaproducto).WithMany(p => p.Productos)
                .HasForeignKey(d => d.LineaproductoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_inventory_film");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
