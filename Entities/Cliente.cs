﻿using System;
using System.Collections.Generic;

namespace EFCore2.Entities;

public partial class Cliente
{
    public int ClienteId { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string? Email { get; set; }

    public virtual ICollection<OrdenVentum> OrdenVenta { get; set; } = new List<OrdenVentum>();
}
